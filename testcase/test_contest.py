import heapq

from solution import *


def tb131_1():
    print(b131_1(nums=[1, 2, 1, 3]))
    print(b131_1(nums=[1, 2, 3]))
    print(b131_1(nums=[1, 2, 2, 1]))


def tb131_2():
    print(b131_2(nums=[1, 3, 1, 7], queries=[1, 3, 2, 4], x=1))
    print(b131_2(nums=[1, 2, 3], queries=[10], x=5))


def tb131_3():
    print(b131_3(limit=4, queries=[[1, 4], [2, 5], [1, 3], [3, 4]]))
    print(b131_3(limit=4, queries=[[0, 1], [1, 2], [2, 2], [3, 4], [4, 5]]))


def tb131_4():
    print(b131_4(queries=[[1, 2], [2, 3, 3], [2, 3, 1], [2, 2, 2]]))
    print(b131_4(queries=[[1, 7], [2, 7, 6], [1, 2], [2, 7, 5], [2, 7, 6]]))


def tw399_1():
    print(w399_1(nums1=[1, 3, 4], nums2=[1, 3, 4], k=1))
    print(w399_1(nums1=[1, 2, 4, 12], nums2=[2, 4], k=3))


def tw399_2():
    print(w399_2())


def tw399_3():
    print(w399_3(nums1=[1, 3, 4], nums2=[1, 3, 4], k=1))
    print(w399_3(nums1=[1, 2, 4, 12], nums2=[2, 4], k=3))


def tw399_3_2():
    print(w399_3_2(nums1=[1, 3, 4], nums2=[1, 3, 4], k=1))
    print(w399_3_2(nums1=[1, 2, 4, 12], nums2=[2, 4], k=3))


def tw399_4():
    print(w399_4())


def tw400_1():
    print(w400_1())


def tw400_2():
    print(w400_2(days=10, meetings=[[5, 7], [1, 3], [9, 10]]))


def tw400_3():
    print(w400_3("abba*"))


def tw400_4():
    print(w400_4(nums=[6], k=2))
    print(w400_4(nums=[1, 8, 1, 5], k=7))
    print(w400_4(nums=[1, 2, 4, 5], k=3))
    print(w400_4(nums=[1, 2, 1, 2], k=2))
    print(w400_4(nums=[1], k=10))


def tw400_4_2():
    print(w400_4_2(nums=[6], k=2))
    print(w400_4_2(nums=[1, 8, 1, 5], k=7))
    print(w400_4_2(nums=[1, 2, 4, 5], k=3))
    print(w400_4_2(nums=[1, 2, 1, 2], k=2))
    print(w400_4_2(nums=[1], k=10))


def tb132_1():
    print(b132_1())


def tb132_2():
    print(b132_2(skills=[2, 5, 4], k=3))


def tb132_3():
    print(b132_3(nums=[1, 2, 1, 1, 3], k=2))
    print(b132_3(nums=[1, 2, 3, 4, 5, 1], k=0))


def tb132_4():
    print(b132_4())


def tw401_1():
    print(w401_1())


def tw401_2():
    print(w401_2(4, 5))
    print(w401_2(1000, 1000))


def tw401_3():
    print(w401_3([1, 1, 3, 3]))
    print(w401_3([1, 6, 4, 3, 2]))
    print(w401_3([1, 5, 4]))


def tw401_4():
    print(w401_4())


def tw403_1():
    print(w403_1())


def tw403_2():
    print(w403_2())


def tw403_3():
    print(w403_3([-14, -13, -20]))


def tw403_4():
    print(w403_4())


def tw404_1():
    print(w404_1(2, 4))
    print(w404_1(10, 1))


def tw404_2():
    print(w404_2())


def tw404_3():
    print(w404_3(nums=[10, 5], k=6))
    print(w404_3(nums=[7, 9], k=8))
    print(w404_3(nums=[1, 2, 3, 4, 5], k=2))
    print(w404_3(nums=[1, 4, 2, 3, 1, 4], k=3))


def tw404_4():
    print(w404_4())


def tw406_1():
    print(w406_1())


def tw406_2():
    print(w406_2())


def tw406_3():
    print(w406_3())


def tw406_4():
    print(w406_4(6, 3, [2, 3, 2, 3, 1], [1, 2]))
    print(w406_4_2(6, 3, [2, 3, 2, 3, 1], [1, 2]))


def tw407_1():
    print(w407_1())


def tw407_2():
    print(w407_2())


def tw407_3():
    print(w407_3())


def tw407_4():
    print(w407_4())


N = 10 ** 6
p = [-1] * (N + 1)
for i in range(2, N + 1):
    if p[i] == -1:
        for num in range(i * i, N + 1, i):
            if p[num] == -1:
                p[num] = i

MOD = 10 ** 9 + 7

MOD = 10 ** 9 + 7


class Solution:
    def sumOfGoodSubsequences(self, nums: List[int]) -> int:
        n = len(nums)
        dp = nums[:]
        for i in range(n):
            for j in range(i):
                if abs(nums[i] - nums[j]) == 1:
                    dp[i] = dp[i] + dp[j] + nums[i]
        print(dp)
        return sum(dp)


if __name__ == "__main__":
    print(Solution().sumOfGoodSubsequences([1, 2, 1]))
