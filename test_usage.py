import bisect
import heapq
import math
from collections import Counter

from solution import gospers_hack


def lcm(numbers):
    def lcm_two(a, b):
        return a * b // math.gcd(a, b)

    result = 1
    for num in numbers:
        result = lcm_two(result, num)
    return result


if __name__ == '__main__':
    s = "asdagfa"
    cnt = Counter[str](s)
    print(min(cnt.values()))
