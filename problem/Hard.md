- [x] [42.接雨水](https://leetcode.cn/problems/trapping-rain-water)
- [x] [233.数字1的个数](https://leetcode.cn/problems/number-of-digit-one/)
- [x] [312.戳气球](https://leetcode.cn/problems/burst-balloons/)
- [x] [410.分割数组的最大值](https://leetcode.cn/problems/split-array-largest-sum/)

    - 最大化最小值/最小化最大值 -> 二分查找
    - [二分答案](https://leetcode.cn/problems/split-array-largest-sum/solutions/2613046/er-fen-da-an-fu-ti-dan-pythonjavacgojsru-n5la/)

- [x] [466.统计重复个数](https://leetcode.cn/problems/count-the-repetitions/)

    - [官方题解](https://leetcode.cn/problems/count-the-repetitions/solutions/208874/tong-ji-zhong-fu-ge-shu-by-leetcode-solution/)
    - 鸽笼原理:
      每过一个s1，对应匹配到的s2的index只有|s2|种可能：0-|s2-1|，所以经过|s2|+1个s1，这个s1结束时匹配到的index必然和前面某个s1结束时匹配到的index相同。进一步，只要index“相同”就能找到循环节。

- [x] [514.自由之路](https://leetcode.cn/problems/freedom-trail/)

    - [动态规划入门](https://b23.tv/72onpYq)

- [x] [551.学生出勤记录 II](https://leetcode.cn/problems/student-attendance-record-ii/)
- [x] [600.不含连续1的非负整数](https://leetcode.cn/problems/non-negative-integers-without-consecutive-ones/)
- [x] [632.最小区间](https://leetcode.cn/problems/smallest-range-covering-elements-from-k-lists/)
- [x] [685.冗余连接 II](https://leetcode.cn/problems/redundant-connection-ii/)
- [x] [699.掉落的方块](https://leetcode.cn/problems/falling-squares/)
- [x] [741.摘樱桃](https://leetcode.cn/problems/cherry-pickup/)
- [x] [815.公交路线](https://leetcode.cn/problems/bus-routes/)
- [x] [857.雇佣K名工人的最低成本](https://leetcode.cn/problems/minimum-cost-to-hire-k-workers/)
- [x] [871.最低加油次数](https://leetcode.cn/problems/minimum-number-of-refueling-stops/)
- [x] [902.最大为N的数字组合](https://leetcode.cn/problems/numbers-at-most-n-given-digit-set/)
- [x] [924.尽量减少恶意软件的传播](https://leetcode.cn/problems/minimize-malware-spread/)
- [x] [987.二叉树的垂序遍历](https://leetcode.cn/problems/vertical-order-traversal-of-a-binary-tree)
- [x] [1235.规划兼职工作](https://leetcode.cn/problems/maximum-profit-in-job-scheduling/)
- [x] [1383.最大的团队表现值](https://leetcode.cn/problems/maximum-performance-of-a-team/)
- [x] [1463.摘樱桃 II](https://leetcode.cn/problems/cherry-pickup-ii/)
- [x] [1483.树节点的第K个祖先](https://leetcode.cn/problems/kth-ancestor-of-a-tree-node/): LCA
- [x] [1515.服务中心的最佳位置](https://leetcode.cn/problems/best-position-for-a-service-centre/)
- [x] [1521.找到最接近目标值的函数值](https://leetcode.cn/problems/best-position-for-a-service-centre/)
- [x] [1542.找出最长的超赞子字符串](https://leetcode.cn/problems/find-longest-awesome-substring/)
- [x] [1547.切棍子的最小成本](https://leetcode.cn/problems/minimum-cost-to-cut-a-stick/)
- [x] [1553.吃掉N个橘子二点最少天数](https://leetcode.cn/problems/minimum-number-of-days-to-eat-n-oranges/)
- [x] [1766.互质数](https://leetcode.cn/problems/tree-of-coprimes/)
- [x] [1793.好子数组的最大分数](https://leetcode.cn/problems/maximum-score-of-a-good-subarray/)
- [x] [1803.统计异或值在范围内的数对有多少](https://leetcode.cn/problems/count-pairs-with-xor-in-a-range/)
- [x] [1835.所有数对按位与结果的异或和](https://leetcode.cn/problems/count-pairs-with-xor-in-a-range/)
- [x] [1883.准时抵达会议现场的最小跳过休息次数](https://leetcode.cn/problems/minimum-skips-to-arrive-at-meeting-on-time/)
- [x] [1944.队列中可以看到的人数](https://leetcode.cn/problems/number-of-visible-people-in-a-queue/)
- [x] [2065.最大化一张图中的路径价值](https://leetcode.cn/problems/selling-pieces-of-wood/)
- [x] [2312.卖木头块](https://leetcode.cn/problems/selling-pieces-of-wood/)
- [x] [2354.优质数对的数目](https://leetcode.cn/problems/number-of-excellent-pairs/)
- [x] [2376.统计特殊整数](https://leetcode.cn/problems/count-special-integers/)

    - [数位DP模板](https://leetcode.cn/problems/count-special-integers/solutions/1746956/shu-wei-dp-mo-ban-by-endlesscheng-xtgx/)

- [x] [2386.找出数组的第K大和](https://leetcode.cn/problems/find-the-k-sum-of-an-array/):二分/堆

    - [0x3f's题解](https://leetcode.cn/problems/find-the-k-sum-of-an-array/solutions/1764389/zhuan-huan-dui-by-endlesscheng-8yiq/)

- [x] [2398.预算内的最多机器人数目](https://leetcode.cn/problems/maximum-number-of-robots-within-budget/description/)
- [x] [2435.矩阵中和能被K整除的路径](https://leetcode.cn/problems/paths-in-matrix-whose-sum-is-divisible-by-k/)
- [x] [2552.统计上升四元组](https://leetcode.cn/problems/count-increasing-quadruplets/)
- [x] [2581.统计可能的树根数目](https://leetcode.cn/problems/count-number-of-possible-root-nodes/): 换根DP
- [x] [2589.完成所有任务的最少时间](https://leetcode.cn/problems/minimum-time-to-complete-all-tasks/)
- [x] [2617.网格图中最少访问的格子数](https://leetcode.cn/problems/minimum-number-of-visited-cells-in-a-grid/)
- [x] [2642.设计可以求最短路径的图类](https://leetcode.cn/problems/design-graph-with-shortest-path-calculator/)
- [x] [2663.字典序最小的美丽字符串](https://leetcode.cn/problems/lexicographically-smallest-beautiful-string/)
- [x] [2713.矩阵中严格递增的单元格数](https://leetcode.cn/problems/maximum-strictly-increasing-cells-in-a-matrix/)
- [x] [2719.统计整数数目](https://leetcode.cn/problems/count-of-integers/)

    - [数位DP](https://leetcode.cn/problems/count-of-integers/solutions/2601111/tong-ji-zheng-shu-shu-mu-by-leetcode-sol-qxqd/)

- [x] [2732.找到矩阵中的好子集](https://leetcode.cn/problems/find-a-good-subset-of-the-matrix/)
- [x] [2742.给墙壁刷油漆](https://leetcode.cn/problems/painting-the-walls/)
- [x] [2809.使数组和小于等于x的最少时间](https://leetcode.cn/problems/minimum-time-to-make-array-sum-at-most-x/)
- [x] [2813.子序列最大优雅度](https://leetcode.cn/problems/maximum-elegance-of-a-k-length-subsequence/)
- [x] [2831.找出最长等值子数组](https://leetcode.cn/problems/find-the-longest-equal-subarray/)
- [x] [2846.边权重均等查询](https://leetcode.cn/problems/minimum-edge-weight-equilibrium-queries-in-a-tree/)

    - LCA
    - [0x3f的题解](https://leetcode.cn/problems/minimum-edge-weight-equilibrium-queries-in-a-tree/solutions/2424060/lca-mo-ban-by-endlesscheng-j54b/):
      其中cnt为三维数组，`cnt[x][i][j]`代表的是x到2^i祖先的所有边中，权重为j的边的个数

- [x] [2867.统计树中的合法路径数目](https://leetcode.cn/problems/count-valid-paths-in-a-tree/): 树DP

    - [0x3f的题解](https://leetcode.cn/problems/count-valid-paths-in-a-tree/solutions/2456716/tu-jie-on-xian-xing-zuo-fa-pythonjavacgo-tjz2/):
      枚举中间

- [x] [2940.找到Alice和Bob可以相遇的建筑](https://leetcode.cn/problems/find-building-where-alice-and-bob-can-meet/)
- [x] [2959.关闭分部的可行集合数目](https://leetcode.cn/problems/number-of-possible-sets-of-closing-branches/)
- [x] [2972.统计移除递增子数组的数目 II](https://leetcode.cn/problems/count-the-number-of-incremovable-subarrays-ii/)
- [x] [3017.按距离统计房屋对数目II](https://leetcode.cn/problems/count-the-number-of-houses-at-a-certain-distance-ii/)
- [x] [3022.给定操作次数内使剩余元素的或值最小](https://leetcode.cn/problems/minimize-or-of-remaining-elements-using-operations/)

    - [拆位, 试填, heq连续子数组合并 from 0x3f](https://leetcode.cn/problems/minimize-or-of-remaining-elements-using-operations/solutions/2622658/shi-tian-fa-pythonjavacgo-by-endlesschen-ysom/)

- [x] [3027.人员站位的方案数 II](https://leetcode.cn/problems/find-the-number-of-ways-to-place-people-ii/)
- [x] [3031.将单词恢复初始状态所需的最短时间 II](https://leetcode.cn/problems/minimum-time-to-revert-word-to-initial-state-ii/)
- [x] [3036.匹配模式数组的子数组数目 II](https://leetcode.cn/problems/number-of-subarrays-that-match-a-pattern-ii/)
- [x] [3041.修改数组后最大化数组中的连续元素数目](https://leetcode.cn/problems/maximize-consecutive-elements-in-an-array-after-modification/)
- [x] [3045.统计前后缀下标对 II](https://leetcode.cn/problems/count-prefix-and-suffix-pairs-ii/)
- [x] [3049.标记所有下标的最早秒数 II](https://leetcode.cn/problems/earliest-second-to-mark-indices-ii/)
- [x] [3068.最大节点价值之和](https://leetcode.cn/problems/find-the-maximum-sum-of-node-values/)
- [x] [3072.将元素分配到两个数组中 II](https://leetcode.cn/problems/distribute-elements-into-two-arrays-ii/): 树状数组
- [x] [3077.K个不相交子数组的最大能量值](https://leetcode.cn/problems/maximum-strength-of-k-disjoint-subarrays/): 划分型DP
- [x] [3082.求出所有子序列的能量和](https://leetcode.cn/problems/find-the-sum-of-the-power-of-all-subsequences/): 背包
- [x] [3086.拾起k个1需要的最少行动次数](https://leetcode.cn/problems/minimum-moves-to-pick-k-ones/): 货仓选址
- [x] [3093.最长公共后缀查询](https://leetcode.cn/problems/longest-common-suffix-queries/): 字典树模板
- [x] [3098.求出所有子序列的能量和](https://leetcode.cn/problems/find-the-sum-of-subsequence-powers/): 相邻相关子序列DP
- [x] [3102.最小化曼哈顿距离](https://leetcode.cn/problems/minimize-manhattan-distances/)
- [x] [3108.带权图里旅途的最小代价](https://leetcode.cn/problems/minimum-cost-walk-in-weighted-graph/)
- [x] [3113.边界元素是最大值的子数组数目](https://leetcode.cn/problems/find-the-number-of-subarrays-where-boundary-elements-are-maximum/)
- [x] [3116.单面值组合的第K小金额](https://leetcode.cn/problems/kth-smallest-amount-with-single-denomination-combination/)
- [x] [3117.划分数组得到最小的值之和](https://leetcode.cn/problems/minimum-sum-of-values-by-dividing-array/)
- [x] [3123.最短路径的边](https://leetcode.cn/problems/find-edges-in-shortest-paths/)
- [x] [3130.找出所有稳定的二进制数组 II](https://leetcode.cn/problems/find-all-possible-stable-binary-arrays-ii/)
- [x] [3134.找出唯一性数组的中位数](https://leetcode.cn/problems/find-the-median-of-the-uniqueness-array/)
- [x] [3139.使数组中所有元素相等的最小开销](https://leetcode.cn/problems/minimum-cost-to-equalize-array/)
- [x] [3154.到达第K级台阶的方案数](https://leetcode.cn/problems/find-number-of-ways-to-reach-the-k-th-stair/)
- [x] [3193.统计逆序对的数目](https://leetcode.cn/problems/count-the-number-of-inversions/)
- [x] [3219.切蛋糕的最小总开销 II](https://leetcode.cn/problems/find-number-of-ways-to-reach-the-k-th-stair/)
- [x] [3551.好子序列的元素之和](https://leetcode.cn/problems/sum-of-good-subsequences/)
